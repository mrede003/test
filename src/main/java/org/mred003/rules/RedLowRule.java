package org.mred003.rules;

import org.mred003.api.AbstractTelemetryEntryRule;
import org.mred003.data.TelemetryEntry;

/**
 * Rule implementation defining the criteria for a "Red Low" rule
 * violation.
 *
 * @author mrede003
 */
public class RedLowRule extends AbstractTelemetryEntryRule {
    public RedLowRule(String component, int violationLimit, long range) {
        super("RED LOW", component, violationLimit, range);
    }

    @Override
    protected boolean isEntryViolation(TelemetryEntry entry) {
        return entry.getValue() < entry.getRedLowLimit();
    }
}
