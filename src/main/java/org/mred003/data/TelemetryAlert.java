package org.mred003.data;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

/**
 * POJO representing a telemetry alert. Stores timestamp as a
 * {@link String} in "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" format.
 *
 * @author mrede003
 */
public class TelemetryAlert {

    private final String satelliteId;

    private final String severity;

    private final String component;

    private final String timestamp;

    /**
     * @param satelliteId Satellite id the alert was generated from.
     * @param severity    Severity classifier of the alert.
     * @param component   Component that caused the alert.
     * @param timestamp   Timestamp the violation that caused the alert took place at.
     * @throws IllegalArgumentException if satelliteId, severity, component, or timestamp are null.
     */
    public TelemetryAlert(String satelliteId, String severity, String component, Long timestamp) {
        if (satelliteId == null || severity == null || component == null || timestamp == null) {
            throw new IllegalArgumentException("String satelliteId, String severity, String component, or Instant " +
                    "timestamp cannot be null");
        }
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        this.timestamp = formatter.format(new Date(timestamp));
    }

    /**
     * Satellite id the alert was generated from.
     *
     * @return satelliteId {@link String}
     */
    public String getSatelliteId() {
        return satelliteId;
    }

    /**
     * Severity of the alert.
     *
     * @return severity {@link String}
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Component that caused the alert.
     *
     * @return component {@link String}
     */
    public String getComponent() {
        return component;
    }

    /**
     * Timestamp the violation causing the alert took place at.
     * Stored as a {@link String} in "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" format.
     *
     * @return timestamp {@link Instant}
     */
    public String getTimestamp() {
        return timestamp;
    }


    @Override
    public String toString() {
        return "TelemetryAlert{" +
                "satelliteId='" + satelliteId + '\'' +
                ", severity='" + severity + '\'' +
                ", component='" + component + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TelemetryAlert)) return false;
        TelemetryAlert that = (TelemetryAlert) o;
        return Objects.equals(satelliteId, that.satelliteId) &&
                Objects.equals(severity, that.severity) &&
                Objects.equals(component, that.component) &&
                Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, severity, component, timestamp);
    }
}
