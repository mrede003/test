package org.mred003.service;

import org.mred003.api.AbstractTelemetryEntryRule;
import org.mred003.api.ITelemetryRulesProcessingService;
import org.mred003.data.TelemetryAlert;
import org.mred003.data.TelemetryEntry;

import java.util.*;

/**
 * Implementation of {@link ITelemetryRulesProcessingService} that processes {@link TelemetryEntry} data
 * against a provided set of {@link AbstractTelemetryEntryRule}s. This implementation verifies rules
 * against {@link TelemetryEntry}s of the same id and should not be used for rules applying to entries of
 * multiple ids.
 *
 * @author mrede003
 */
public class SingleIdTelemetryRulesProcessingService implements ITelemetryRulesProcessingService {

    /**
     * Map of telemetry entry ids to sorted telemetry entries.
     */
    private final Map<String, SortedSet<TelemetryEntry>> idToSortedEntries = new HashMap<>();

    /**
     * Set of rules to be applies to stored telemetry entries.
     */
    private final Set<AbstractTelemetryEntryRule> rules = new HashSet<>();

    @Override
    public void putTelemetryData(Set<TelemetryEntry> entries) {
        if(entries == null) {
            System.out.println("Cannot put null entry set. Fail fast.");
            return;
        }
        // Filter by id since this implementation only deals rules applying to the same id.
        entries.forEach(telemetryEntry -> {
            if (idToSortedEntries.containsKey(telemetryEntry.getId())) {
                idToSortedEntries.get(telemetryEntry.getId()).add(telemetryEntry);
            } else {
                // Sort by in ascending order by timestamp.
                SortedSet<TelemetryEntry> sortedEntries = new TreeSet<>(Comparator.comparingLong(TelemetryEntry::getTimeStamp));
                sortedEntries.add(telemetryEntry);
                idToSortedEntries.put(telemetryEntry.getId(), sortedEntries);
            }
        });
    }

    @Override
    public void putRule(AbstractTelemetryEntryRule rule) {
        if(rule == null) {
            System.out.println("Cannot put null rule. Fail fast.");
            return;
        }
        rules.add(rule);
    }

    @Override
    public Set<TelemetryAlert> processData() {
        Set<TelemetryAlert> ruleViolationAlerts = new HashSet<>();
        // For each rule.
        rules.forEach(rule -> {
            // For each id.
            idToSortedEntries.forEach((key, value) -> {
                // Cheap case.
                if (value.size() < rule.getLimit()) {
                    return;
                }
                List<TelemetryEntry> entriesList = new ArrayList<>(value);
                int front = 0;
                int lastIndex = entriesList.size() - 1;
                // Walk backwards until you have a set of data within range.
                for (int back = lastIndex; front < back; back--) {
                    if (isDataWithinInterval(entriesList.get(front), entriesList.get(back), rule)) {
                        // Pass data set into rule for evaluation.
                        TelemetryEntry violation = rule.getViolation(entriesList.subList(front, back + 1));
                        // If there was a violation generate alert.
                        if (violation != null) {
                            ruleViolationAlerts.add(getAlertFromEntryAndRule(violation, rule));
                        }
                        // If at end of data set break, otherwise reset.
                        if (back == lastIndex) {
                            break;
                        } else {
                            back = lastIndex;
                            front++;
                        }
                    }
                }
            });
        });
        return ruleViolationAlerts;
    }

    /**
     * Helper function to determine if two {@link TelemetryEntry}s are within a
     * {@link AbstractTelemetryEntryRule}'s range.
     *
     * @param entry1 {@link TelemetryEntry}
     * @param entry2 {@link TelemetryEntry}
     * @param rule   {@link AbstractTelemetryEntryRule}
     * @return true if the two entires are in range, false if otherwise.
     */
    private boolean isDataWithinInterval(TelemetryEntry entry1, TelemetryEntry entry2, AbstractTelemetryEntryRule rule) {
        return (entry2.getTimeStamp() - entry1.getTimeStamp() <= rule.getRange());
    }

    /**
     * Helper function to generate a {@link TelemetryAlert} from a {@link AbstractTelemetryEntryRule}
     * and the violating {@link TelemetryEntry}.
     *
     * @param entry {@link TelemetryEntry}
     * @param rule  {@link AbstractTelemetryEntryRule}
     * @return {@link TelemetryAlert}
     */
    private TelemetryAlert getAlertFromEntryAndRule(TelemetryEntry entry, AbstractTelemetryEntryRule rule) {
        return new TelemetryAlert(entry.getId(), rule.getSeverity(), entry.getComponent(), entry.getTimeStamp());
    }
}
