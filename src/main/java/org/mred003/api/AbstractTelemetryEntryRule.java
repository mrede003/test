package org.mred003.api;

import org.mred003.data.TelemetryEntry;

import java.util.List;

/**
 * Generic behavior associated with a rule to be applied to
 * {@link TelemetryEntry} objects. This class only returns
 * the first rule violation in a given data set.
 *
 * @author mrede003
 */
public abstract class AbstractTelemetryEntryRule {

    protected final int violationLimit;
    private final String severity;
    private final String component;
    private final long range;

    /**
     * Constructor.
     *
     * @param severity Message representing the classifier of a rule violation.
     * @param component Component this rule applies to.
     * @param violationLimit Number of violations that defines a rule
     *                       being broken.
     * @param range          Range of time in epoch millis a data set to be
     *                       checked for violations is to be constrained to.
     */
    public AbstractTelemetryEntryRule(String severity, String component, int violationLimit, long range) {
        this.severity = severity;
        this.component = component;
        this.violationLimit = violationLimit;
        this.range = range;
    }

    /**
     * API to determine if  {@link TelemetryEntry} is in violation
     * of the rule implementation.
     *
     * @param entry The {@link TelemetryEntry} to be checked.
     * @return true if the {@link TelemetryEntry} is in violation
     * of the rule, false if otherwise.
     */
    protected abstract boolean isEntryViolation(TelemetryEntry entry);

    /**
     * Function that returns the first {@link TelemetryEntry} that
     * violates the rule implementation in a list of entries. This
     * function is agnostic of ids associated with the given list of
     * entries such that it does verify id equality.
     *
     * @param entriesWithinRange The list of {@link TelemetryEntry} data to be checked.
     * @return {@link TelemetryEntry} the first violating entry in a list of entries.
     * Returns null if no rule violation is found.
     */
    public TelemetryEntry getViolation(List<TelemetryEntry> entriesWithinRange) {
        // Cheap cases.
        if(entriesWithinRange == null) {
            System.out.println("Entries set was null. Returning null.");
            return null;
        }
        // If the size of the data set is less than what violates a rule.
        if (entriesWithinRange.size() < violationLimit) {
            return null;
        }
        // If data set is outside range of time.
        if (entriesWithinRange.get(entriesWithinRange.size() - 1).getTimeStamp() -
                entriesWithinRange.get(0).getTimeStamp() > range) {
            System.out.println("List of entry's time range is outside of rule definition. Returning null.");
            return null;
        }

        TelemetryEntry violation = null;
        int violations = 0;

        // Check if entry has defined rule component and violates rule.
        for (int i = 0; i < entriesWithinRange.size(); i++) {
            if (isSameComponent(entriesWithinRange.get(i)) && isEntryViolation(entriesWithinRange.get(i))) {
                violations++;
                // Only return the first violation even if the set contains more than violationLimit amount.
                if (violations == 1) {
                    violation = entriesWithinRange.get(i);
                } else if (violations == violationLimit) {
                    return violation;
                }
            }
        }
        // No violation found.
        return null;
    }

    /**
     * The number of violations that defines a rule being broken.
     *
     * @return int
     */
    public int getLimit() {
        return violationLimit;
    }

    /**
     * The range of time in epoch millis a data set checked for
     * violations is to be constrained to.
     *
     * @return int
     */
    public long getRange() {
        return range;
    }

    /**
     * Message representing the classifier of a rule violation.
     *
     * @return {@link String}
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * Helper function to compare component equality between a given
     * {@link TelemetryEntry} and the rule implementation.
     *
     * @param entry {@link TelemetryEntry} The given entry.
     * @return true if the components are equal, false if otherwise.
     */
    private boolean isSameComponent(TelemetryEntry entry) {
        return component.equals(entry.getComponent());
    }
}