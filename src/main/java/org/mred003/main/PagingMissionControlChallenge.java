package org.mred003.main;

import org.mred003.api.AbstractTelemetryEntryRule;
import org.mred003.api.ITelemetryRulesProcessingService;
import org.mred003.rules.RedHighRule;
import org.mred003.rules.RedLowRule;
import org.mred003.service.SingleIdTelemetryRulesProcessingService;
import org.mred003.utils.SerializationUtils;

import java.io.File;
import java.util.List;

/**
 * Entry point for Enlighten Paging Mission Control Challenge.
 *
 * @author matthew.redenius
 */
public class PagingMissionControlChallenge {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("No arguments provided. Please provide a file and try again!.");
            return;
        }

        // Init.
        ITelemetryRulesProcessingService telemetryRulesProcessingService = new SingleIdTelemetryRulesProcessingService();
        AbstractTelemetryEntryRule redLowRule = new RedLowRule("BATT", 3, 300000);
        AbstractTelemetryEntryRule redHighRule = new RedHighRule("TSTAT", 3, 300000);

        // Parse input file and add data.
        telemetryRulesProcessingService.putTelemetryData(SerializationUtils.deserializeTelemetryEntryFromFile(new File(args[0])));

        // Add Rules.
        telemetryRulesProcessingService.putRule(redLowRule);
        telemetryRulesProcessingService.putRule(redHighRule);

        // Process Data and serialize result to JSON.
        List<String> jsonAlertStrings = SerializationUtils.serializeAlertsToJson(telemetryRulesProcessingService.processData());

        // Output to console.
        jsonAlertStrings.forEach(s -> System.out.println(s));
    }
}